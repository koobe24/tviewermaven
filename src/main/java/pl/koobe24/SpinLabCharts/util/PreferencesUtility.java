package pl.koobe24.SpinLabCharts.util;

import java.util.prefs.Preferences;

/**
 * Created by tomas on 05.11.2016.
 */
public class PreferencesUtility {

    private final static String SCREEN = "screen";
    private final static String WIDTH = "width";
    private final static String HEIGHT = "height";
    private final static String COMPACT = "compact";
    private final static String COMPACT_WIDTH = "compact_width";
    private final static String COMPACT_HEIGHT = "compact_height";
    private final static String WHERE_X = "x";
    private final static String WHERE_Y = "y";
    private final static String DARK_THEME = "dark_theme";
    private final Preferences preferences;

    public PreferencesUtility() {
        preferences = Preferences.userNodeForPackage(this.getClass());
    }

    public int getScreenIndex() {
        return preferences.getInt(SCREEN, 0);
    }

    public double getWidth() {
        return preferences.getDouble(WIDTH, 800.0);
    }

    public double getHeight() {
        return preferences.getDouble(HEIGHT, 600.0);
    }

    public boolean getIsCompactMode() {
        return preferences.getBoolean(COMPACT, false);
    }

    public double getCompactModeWidth() {
        return preferences.getDouble(COMPACT_WIDTH, 300.0);
    }

    public double getCompactModeHeight() {
        return preferences.getDouble(COMPACT_HEIGHT, 200.0);
    }

    public double getX() {
        return preferences.getDouble(WHERE_X, 0.0);
    }

    public double getY() {
        return preferences.getDouble(WHERE_Y, 0.0);
    }

    public boolean getIsDarkTheme() {
        return preferences.getBoolean(DARK_THEME, false);
    }

    public void setScreen(int screen) {
        preferences.putInt(SCREEN, screen);
    }

    public void setWidth(double width) {
        preferences.putDouble(WIDTH, width);
    }

    public void setHeight(double height) {
        preferences.putDouble(HEIGHT, height);
    }

    public void setIsCompactMode(boolean isCompactMode) {
        preferences.putBoolean(COMPACT, isCompactMode);
    }

    public void setCompactWidth(double width) {
        preferences.putDouble(COMPACT_WIDTH, width);
    }

    public void setCompactHeight(double height) {
        preferences.putDouble(COMPACT_HEIGHT, height);
    }

    public void setX(double x) {
        preferences.putDouble(WHERE_X, x);
    }

    public void setY(double y) {
        preferences.putDouble(WHERE_Y, y);
    }

    public void setIsDarkTheme(boolean isDarkTheme) {
        preferences.putBoolean(DARK_THEME, isDarkTheme);
    }
}
