package pl.koobe24.SpinLabCharts.util;

import javafx.scene.image.Image;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by tomas on 05.11.2016.
 */
public class ImageKeeper {
    private List<Image> images;
    private List<String> paths;

    public enum Position {
        BU("/bu/"), SB3H("/sb3h/"), BB3H("/bb3h/"), SBHU("/sbhu/"), BBHU("/bbhu/"), OTHER("/other/");

        private final String directory;

        Position(String directory) {
            this.directory = directory;
        }

        public String getDirectory() {
            return directory;
        }
    }


    public ImageKeeper(Position position) {
        loadPaths(position);
        removeBadPaths();
        sortPaths();
        loadFromDisk();
    }

    private void loadPaths(Position position) {
        paths = new ArrayList<>();
        Path directoryToWalk = Paths.get(System.getProperty("user.dir") + position.getDirectory());
        try (Stream<Path> pathsStream = Files.walk(directoryToWalk)) {
            pathsStream.forEach(filePath -> {
                if (isPathToImage(filePath)) {
                    paths.add(filePath.toString());
                }
            });
        } catch (IOException e) {
            //todo czy cos z exception?
            if (e instanceof NoSuchFileException) {
                System.err.println("brak plikow dla " + position.toString());
            }
        }
    }

    private void removeBadPaths() {
        Pattern pattern = Pattern.compile(".+[0-9]+_.+");
        paths.forEach(path -> {
            Matcher matcher = pattern.matcher(path);
            if (!matcher.matches()) {
                paths.remove(path);
            }
        });
    }

    private boolean isPathToImage(Path filePath) {
        String lowerCase = filePath.toString().toLowerCase();
        boolean isImage = lowerCase.endsWith(".jpg") || lowerCase.endsWith(".jpeg") || lowerCase.endsWith(".png");
        return Files.isRegularFile(filePath) && isImage;
    }

    private void sortPaths() {
        Collections.sort(this.paths, (o1, o2) -> {
            String p1 = o1.substring(o1.lastIndexOf(File.separatorChar) + 1, o1.indexOf('_'));
            String p2 = o2.substring(o2.lastIndexOf(File.separatorChar) + 1, o2.indexOf('_'));
            return Integer.parseInt(p1) - Integer.parseInt(p2);
        });
    }

    private void loadFromDisk() {
        images = new ArrayList<>();
        for (String s : paths) {
            Image img = new Image("file:" + s);
            images.add(img);
        }
    }

    public Image getImageByPath(String s) {
        return images.get(paths.indexOf(s));
    }

    public List<String> getPaths() {
        return paths;
    }

}
