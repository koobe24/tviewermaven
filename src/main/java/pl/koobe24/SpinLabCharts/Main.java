package pl.koobe24.SpinLabCharts;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import pl.koobe24.SpinLabCharts.logging.Log4jBackstop;
import pl.koobe24.SpinLabCharts.util.PreferencesUtility;

import java.io.IOException;

@Slf4j
public class Main extends Application {

    private PreferencesUtility preferencesUtility;
    private Stage primaryStage;
    private Scene primaryScene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.info("Started application.");
        log.info("Working directory: " + System.getProperty("user.dir"));
        Thread.setDefaultUncaughtExceptionHandler(new Log4jBackstop());
        preferencesUtility = new PreferencesUtility();
        this.primaryStage = primaryStage;
        initializeWindow();
    }

    private void initializeWindow() throws Exception {
        log.info("Starting initializing window.");
        setBasicStageProperties();
        createRightModeScene();
        setRightStylesheet();
        primaryStage.setScene(primaryScene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> {
            saveWidthHeight();
            saveXY();
            primaryStage.hide();
        });
    }

    private void setBasicStageProperties() {
        log.info("Setting basic properties.");
        primaryStage.setTitle("SpinLab Charts");
        primaryStage.setResizable(true);

        double x = preferencesUtility.getX();
        if (!isXCorrect(x)) {
            x = 0;
            preferencesUtility.setX(0);
        }
        double y = preferencesUtility.getY();
        primaryStage.setX(x);
        primaryStage.setY(y);
    }

    private boolean isXCorrect(double x) {
        boolean isCorrect = false;
        for (Screen screen : Screen.getScreens()) {
            if (x >= screen.getBounds().getMinX() && x <= screen.getBounds().getMaxX()) {
                isCorrect = true;
            }
        }
        return isCorrect;
    }

    private void createRightModeScene() throws IOException {
        double width;
        double height;
        Parent root;
        if (preferencesUtility.getIsCompactMode()) {
            log.info("Creating scene in compact mode.");
            width = preferencesUtility.getCompactModeWidth();
            height = preferencesUtility.getCompactModeHeight();
            root = FXMLLoader.load(this.getClass().getClassLoader().getResource("compact.fxml"));
        } else {
            log.info("Creating scene in normal mode.");
            width = preferencesUtility.getWidth();
            height = preferencesUtility.getHeight();
            root = FXMLLoader.load(this.getClass().getClassLoader().getResource("layout.fxml"));
        }
        primaryScene = new Scene(root, width, height);
        log.info("Scene created.");
    }

    private void setRightStylesheet() {
        if (preferencesUtility.getIsDarkTheme()) {
            log.info("Setting dark theme.");
            primaryScene.getStylesheets().add(this.getClass().getClassLoader().getResource("dark_style.css").toExternalForm());
        } else {
            log.info("Setting normal theme.");
            primaryScene.getStylesheets().add(this.getClass().getClassLoader().getResource("default.css").toExternalForm());
        }
    }

    private void saveWidthHeight() {
        double width = primaryScene.getWidth();
        double height = primaryScene.getHeight();
        if (preferencesUtility.getIsCompactMode()) {
            preferencesUtility.setCompactWidth(width);
            preferencesUtility.setCompactHeight(height);
        } else {
            preferencesUtility.setWidth(width);
            preferencesUtility.setHeight(height);
        }
        log.info("Saved width:" + width + " and height:" + height);
    }

    private void saveXY() {
        preferencesUtility.setX(primaryStage.getX());
        preferencesUtility.setY(primaryStage.getY());
        log.info("Saved X:" + primaryStage.getX() + " and Y:" + primaryStage.getY());
    }


    public static void main(String[] args) {
        launch(args);
    }
}
