package pl.koobe24.SpinLabCharts.logging;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by tomas on 16.12.2016.
 */
@Slf4j
public class Log4jBackstop implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        log.error("Uncaught exception in thread: " + t.getName(), e);
    }
}
