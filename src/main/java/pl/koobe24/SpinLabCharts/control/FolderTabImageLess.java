package pl.koobe24.SpinLabCharts.control;

import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by tomas on 08.11.2016.
 */
public class FolderTabImageLess extends CustomTab {

    private GridPane buttonsGridPane;

    public void initialize() {
        createImageKeeper();
        createButtonsGridPane();
        createButtonsScrollPane();
        createMainAnchorPane();
        createButtons();
    }

    private void createButtonsGridPane() {
        buttonsGridPane = new GridPane();
        buttonsGridPane.setMinHeight(0.0);
        buttonsGridPane.setMinWidth(0.0);
    }

    private void createButtonsScrollPane() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        scrollPane.setContent(buttonsGridPane);
        this.setContent(scrollPane);
    }

    private void createMainAnchorPane() {
        AnchorPane mainAnchorPane = new AnchorPane();
        mainAnchorPane.setMinHeight(0);
        mainAnchorPane.setMinWidth(0);
        mainAnchorPane.getChildren().add(buttonsGridPane);
        AnchorPane.setTopAnchor(buttonsGridPane, 0.0);
        AnchorPane.setBottomAnchor(buttonsGridPane, 0.0);
        AnchorPane.setRightAnchor(buttonsGridPane, 0.0);
        AnchorPane.setLeftAnchor(buttonsGridPane, 0.0);
    }

    private Image getImageForButton(String path) {
        return imageKeeper.getImageByPath(path);
    }

    private void createButtons() {
        int noOfButtons = imageKeeper.getPaths().size();
        int i = 0, j = 0;

        ToggleGroup group = new ToggleGroup();
        for (String path : imageKeeper.getPaths()) {
            String name = createButtonName(path);
            ToggleButton button = createSingleButton(name);
            button.setToggleGroup(group);
            buttonsGridPane.add(button, j, i);
            i++;
            if (i == (noOfButtons + 1) / 2) {
                j++;
                i = 0;
            }

            Image image = getImageForButton(path);
            addButtonHandler(button, image);
        }
    }

    private ToggleButton createSingleButton(String name) {
        ToggleButton button = new ToggleButton(name);
        button.setMaxWidth(Double.MAX_VALUE);
        button.setMinHeight(30.0);
        return button;
    }

    private void addButtonHandler(ToggleButton button, Image image) {
        button.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                StackPane popupWindowStackPane = createPopupStackPaneWithImage(image);
                Scene scene = createSceneForPopup(popupWindowStackPane);
                replacePopupStage();
                popupStage.setScene(scene);
                popupStage.show();

                popupWindowStackPane.setOnMouseClicked(event -> {
                    button.setSelected(false);
                    popupStage.hide();
                });
            } else {
                popupStage.hide();
            }
        });
    }

    private StackPane createPopupStackPaneWithImage(Image image) {
        StackPane popupWindowStackPane = new StackPane();
        ImageView popupImageView = new ImageView(image);
        popupWindowStackPane.getChildren().add(popupImageView);
        return popupWindowStackPane;
    }

    private void replacePopupStage() {
        popupStage.hide();
        popupStage = new Stage();
        popupStage.initStyle(StageStyle.UNDECORATED);
        Rectangle2D bounds = getScreenBounds();
        popupStage.setX(bounds.getMinX());
        popupStage.setY(bounds.getMinY());
        popupStage.setMaximized(true);
    }

    private Scene createSceneForPopup(StackPane stackPane) {
        Scene scene = new Scene(stackPane);
        String activeStylesheet = buttonsGridPane.getScene().getStylesheets().get(0);
        scene.getStylesheets().add(activeStylesheet);
        return scene;
    }
}
