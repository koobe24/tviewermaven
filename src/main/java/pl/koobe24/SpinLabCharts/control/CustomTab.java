package pl.koobe24.SpinLabCharts.control;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Tab;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import pl.koobe24.SpinLabCharts.util.ImageKeeper;
import pl.koobe24.SpinLabCharts.util.PreferencesUtility;

import java.io.File;

/**
 * Created by tomas on 18.11.2016.
 */
@Slf4j
public abstract class CustomTab extends Tab {
    Stage popupStage = new Stage();
    ImageKeeper imageKeeper;
    protected StringProperty position = new SimpleStringProperty();
    private ImageKeeper.Position positionEnum;

    void createImageKeeper() {
        log.info("Creating ImageKeeper for " + positionEnum);
        imageKeeper = new ImageKeeper(positionEnum);
    }

    //Do inicjalizacji z fxml
    public final String getPosition() {
        return position.get();
    }

    public final void setPosition(String position) {
        this.position.set(position);
        positionEnum = ImageKeeper.Position.valueOf(position);
    }

    String createButtonName(String path) {
        log.info("Creating button name for path: " + path);
        String fullName = getFullFileNameFromPath(path);
        log.info("Full file name from path: " + fullName);
        int nameStart = fullName.indexOf('_') + 1;
        return fullName.substring(nameStart);
    }

    private String getFullFileNameFromPath(String path) {
        int substringStart = path.lastIndexOf(File.separatorChar) + 1;
        int substringEnd = path.lastIndexOf('.');
        return path.substring(substringStart, substringEnd);
    }

    Rectangle2D getScreenBounds() {
        log.info("Reading screen bounds.");
        Screen screen = getScreenFromProperties();
        return screen.getBounds();
    }

    private Screen getScreenFromProperties() {
        int screenIndex = getCorrectIndex();
        return Screen.getScreens().get(screenIndex);
    }

    private int getCorrectIndex() {
        log.info("Reading correct screen index.");
        PreferencesUtility preferencesUtility = new PreferencesUtility();
        int screenIndex = preferencesUtility.getScreenIndex();
        if (Screen.getScreens().size() <= screenIndex) {
            preferencesUtility.setScreen(0);
            screenIndex = 0;
        }
        return screenIndex;
    }

    abstract public void initialize();
}
