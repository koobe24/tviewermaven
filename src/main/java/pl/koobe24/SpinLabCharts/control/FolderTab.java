package pl.koobe24.SpinLabCharts.control;

import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by tomas on 05.11.2016.
 */
@Slf4j
public class FolderTab extends CustomTab {

    private final double DIVIDER_POSITION = 0.3;

    private VBox buttonsVbox;
    private SplitPane mainSplitPane;
    private ScrollPane mainScrollPane;
    private ScrollPane imageScrollPane;
    private StackPane buttonsStackPane;

    public void initialize() {
        log.info("Initializing folder tab.");
        createImageKeeper();
        createSplitPane();
        createMainAnchorPane();
        createVbox();
        createBindedMainScrollPane();
        bindVboxWidth();
        createImageScrollPane();
        createStackPane();
        createButtons();
    }


    private void createSplitPane() {
        log.info("Creating SplitPane.");
        mainSplitPane = new SplitPane();
        mainSplitPane.setMinHeight(0);
        mainSplitPane.setMinWidth(0);
    }

    private void createMainAnchorPane() {
        log.info("Creating AnchorPane.");
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setMinHeight(0);
        anchorPane.setMinWidth(0);

        anchorPane.getChildren().add(mainSplitPane);
        AnchorPane.setTopAnchor(mainSplitPane, 0.0);
        AnchorPane.setBottomAnchor(mainSplitPane, 0.0);
        AnchorPane.setRightAnchor(mainSplitPane, 0.0);
        AnchorPane.setLeftAnchor(mainSplitPane, 0.0);

        this.setContent(anchorPane);
    }

    private void createVbox() {
        buttonsVbox = new VBox();
    }


    private void createBindedMainScrollPane() {
        mainScrollPane = createMainScrollPane();
        setMainScrollPaneParentAndChild();
        mainScrollPane.minWidthProperty().bind(buttonsVbox.minWidthProperty());
        mainScrollPane.maxWidthProperty().bind(mainSplitPane.widthProperty().multiply(DIVIDER_POSITION));
    }

    private void setMainScrollPaneParentAndChild() {
        mainScrollPane.setContent(buttonsVbox);
        mainSplitPane.getItems().add(mainScrollPane);
    }

    private ScrollPane createMainScrollPane() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        return scrollPane;
    }

    private void bindVboxWidth() {
        buttonsVbox.maxWidthProperty().bind(mainScrollPane.widthProperty());
    }

    private void createImageScrollPane() {
        imageScrollPane = new ScrollPane();
        imageScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        imageScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        mainSplitPane.getItems().add(imageScrollPane);
    }

    private void createStackPane() {
        buttonsStackPane = new StackPane();
        buttonsStackPane.setMinHeight(0.0);
        buttonsStackPane.setMinWidth(0);
        imageScrollPane.setContent(buttonsStackPane);
        buttonsStackPane.getChildren().add(new ImageView());
    }

    private void createButtons() {
        ToggleGroup group = new ToggleGroup();
        for (String path : imageKeeper.getPaths()) {
            String buttonName = createButtonName(path);
            ToggleButton button = createButtonForImage(buttonName);
            button.setToggleGroup(group);
            addHandlerToButton(button, path);
        }
    }

    private ToggleButton createButtonForImage(String name) {
        ToggleButton button = new ToggleButton(name);
        button.setMaxWidth(Double.MAX_VALUE);
        button.setMinHeight(30.0);
        buttonsVbox.getChildren().add(button);
        return button;
    }

    private void addHandlerToButton(ToggleButton button, String path) {
        button.setOnAction(event -> {
            ImageView view = createImageViewForPath(path);
            addImageViewHandler(view);
        });
    }

    private ImageView createImageViewForPath(String path) {
        Image image = imageKeeper.getImageByPath(path);
        ImageView view = new ImageView(image);
        StackPane.setAlignment(view, Pos.CENTER);
        buttonsStackPane.getChildren().set(0, view);
        return view;
    }

    private void addImageViewHandler(ImageView view) {
        view.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                StackPane popupWindowStackPane = createStackPaneWithImage(view.getImage());

                popupStage.hide();
                createNewPopupStage();
                Scene scene = createSceneForPopup(popupWindowStackPane);
                popupStage.setScene(scene);
                popupStage.setMaximized(true);
                popupStage.show();

                addPopupPaneHandler(popupWindowStackPane);
            }
        });
    }

    private StackPane createStackPaneWithImage(Image image) {
        StackPane popupWindowStackPane = new StackPane();
        ImageView pom = new ImageView(image);
        popupWindowStackPane.getChildren().add(pom);
        return popupWindowStackPane;
    }

    private void createNewPopupStage() {
        popupStage = new Stage();
        popupStage.initStyle(StageStyle.UNDECORATED);
        Rectangle2D bounds = getScreenBounds();
        popupStage.setX(bounds.getMinX());
        popupStage.setY(bounds.getMinY());
        popupStage.setMaximized(true);
    }

    private Scene createSceneForPopup(StackPane stackPane) {
        Scene scene = new Scene(stackPane);
        String activeStylesheet = mainScrollPane.getScene().getStylesheets().get(0);
        scene.getStylesheets().add(activeStylesheet);
        return scene;
    }

    private void addPopupPaneHandler(StackPane stackPane) {
        stackPane.setOnMouseClicked(event -> popupStage.hide());
    }
}
