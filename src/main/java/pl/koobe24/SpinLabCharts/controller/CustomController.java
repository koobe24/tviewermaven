package pl.koobe24.SpinLabCharts.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import pl.koobe24.SpinLabCharts.Main;
import pl.koobe24.SpinLabCharts.control.CustomTab;
import pl.koobe24.SpinLabCharts.util.PreferencesUtility;

import java.net.URL;
import java.util.ListIterator;
import java.util.ResourceBundle;

/**
 * Created by tomasz on 20.11.16.
 */
public class CustomController implements Initializable {

    @FXML
    private Menu screenMenu;

    @FXML
    private TabPane tabPane;

    @FXML
    private CheckMenuItem darkTheme;

    @FXML
    private MenuItem closeMenu;

    @FXML
    private CheckMenuItem compactMode;

    private final PreferencesUtility preferencesUtility;

    public CustomController() {
        preferencesUtility = new PreferencesUtility();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTabs();
        setCloseButtonHandler();
        addScreensMenu();
        chooseScreen();
        setCompactButtonOnAction();
        compactMode.setSelected(preferencesUtility.getIsCompactMode());
        darkTheme.setSelected(preferencesUtility.getIsDarkTheme());
        addDarkThemeListener();
    }

    private void addScreensMenu() {
        ToggleGroup screenToggleGroup = new ToggleGroup();
        int i = 1;
        for (Screen screen : Screen.getScreens()
                ) {
            RadioMenuItem item = new RadioMenuItem("Screen " + i);
            item.setToggleGroup(screenToggleGroup);
            screenMenu.getItems().add(item);
            i++;
            item.setOnAction(event -> {
                int screenIndex = Screen.getScreens().indexOf(screen);
                preferencesUtility.setScreen(screenIndex);
            });
        }
    }

    private void addDarkThemeListener() {
        darkTheme.selectedProperty().addListener((observable, oldValue, newValue) -> {
            try {
                preferencesUtility.setIsDarkTheme(newValue);
                Main app = new Main();
                app.start(new Stage());
                tabPane.getScene().getWindow().hide();
            } catch (Exception e) {
                System.err.println("Blad przy tworzeniu okna przy zmianie wygladu");
            }
        });
    }

    private void initializeTabs() {
        ObservableList<Tab> tabList = tabPane.getTabs();
        ListIterator<Tab> it = tabList.listIterator();
        ((CustomTab) it.next()).initialize();
        while (it.hasNext()) {
            CustomTab t = (CustomTab) it.next();
            Platform.runLater(t::initialize);
        }
    }

    private void setCloseButtonHandler() {
        closeMenu.setOnAction(event -> Platform.exit());
    }

    private void chooseScreen() {
        int screenIndex = preferencesUtility.getScreenIndex();
        screenIndex = Screen.getScreens().size() > screenIndex ? screenIndex : 0;
        RadioMenuItem selectedScreen = (RadioMenuItem) screenMenu.getItems().get(screenIndex);
        selectedScreen.setSelected(true);
    }

    private void setCompactButtonOnAction() {
        compactMode.setOnAction(event -> {
            try {
                preferencesUtility.setIsCompactMode(!preferencesUtility.getIsCompactMode());
                Main app = new Main();
                app.start(new Stage());
                tabPane.getScene().getWindow().hide();
            } catch (Exception e) {
                System.err.println("Blad przy tworzeniu okna przy zmianie trybu");
            }
        });
    }


}
